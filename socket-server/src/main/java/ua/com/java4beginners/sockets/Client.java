package ua.com.java4beginners.sockets;

import java.io.OutputStream;
import java.net.Socket;

public class Client {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("localhost", 6000);
            OutputStream out = socket.getOutputStream();
            out.write("Hello to Server from Client.".getBytes());
            out.flush();
            out.close();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
