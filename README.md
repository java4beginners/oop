# oop

### Serializer

Tasks:

- Add class Rectangle with fields int x, y, height, width.
- Implement XmlRectangleSerializer. JUnit test is required.
- Add class Point(int x, y)
- Implement XmlPointSerializer, with tests of course.
- Add class Triangle, with fields Point first, second, third;
- Implement XmlTriangleSerializer utilizing XmlPointSerializer
- Add interface `Shape` with method `String getType();`.
- Make all shapes impelemts `Shape` interface.
- Introduce new interface `Serializer<T extends Shape>` with method `void serialize(T shape, OutputStream out);`.
- Make all serializers implements interface `Serializer`.
- Add class `Group` that implements `Shape` and contains list of other Shapes inside.
- Implement `XmlGroupSerializer`.
- Create abstract class that implemet common logic for all serializers. This a Template Method design pattern.
 
*JSON*

- Implement all serializers for JSON format (`Json***Serializer`) for the same shapes.
- Implemet all serializers for binary format(`Binary***Serializer`).
- Implemet factory method that creates serializers for spesified format.
 
*Binary*

- Implement classes that do serialization in binary format.  
  First byte represent a shape type. e.g. for Point it's `1`, Circle - `2`, and so on.   
  Second byte - it's size of current shape in bytes. So after this number of bytes we can find the next shape.  
  Then - fields values of current shape.

### Socket Server

Tasks:

- Run Server part, run client part.
- Make Server to run endless and respond to all incoming requests.
- Make Echo Server that return incoming message to client side.